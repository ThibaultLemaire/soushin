using System;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using System.Threading;

namespace Soushin
{
	/// <summary>
	/// Window that handles displaying the application's main views
	/// </summary>
	public partial class MainWindow : Gtk.Window
	{
		/// <summary>
		/// Controller for this window
		/// </summary>
		private readonly WindowController controller;

		/// <summary>
		/// A list of the UI elements handling entries and editing them (UI for the Change class)
		/// </summary>
		private List<EditEntry> edits;
		/// <summary>
		/// A list of the UI elements used by the study view to present questions
		/// </summary>
		private List<StudyView> studies;
		/// <summary>
		/// A list of the UI elements used by the test view to present questions
		/// </summary>
		private List<TestView> tests;
		/// <summary>
		/// A list of the UI elements used by the score view to score questions
		/// </summary>
		private List<ScoreView> scores;

		/// <summary>
		/// A list of the intermediary Change class that represents 
		/// </summary>
		private List<Change> changes;

		/// <summary>
		/// Current test question's index
		/// </summary>
		private int testIndex;
		/// <summary>
		/// Current study question's index
		/// </summary>
		private int studyIndex;

		/// <summary>
		/// True if the current mode is test mode, false if it is study mode.
		/// </summary>
		private bool test;
		/// <summary>
		/// True if changes have been made from the last save, false otherwise
		/// </summary>
		private bool changed;

		public MainWindow()
			: base(Gtk.WindowType.Toplevel)
		{
			this.controller = new WindowController();
			Build();

			this.hidePanels();
			this.panelMain.Show();
		}
		/// <summary>
		/// Checks the UI components for any entries that have been deleted and updates the change records
		/// </summary>
		private void processDeletes()
		{
			for (int i = 0; i < this.edits.Count; i++)
			{
				if (this.edits[i].Deleted)
				{
					this.changed = true;
					this.changes[i].Delete();
				}
			}
		}
		/// <summary>
		/// Does final preparation of the change records and submits them to the controller for incorporation into the data model
		/// </summary>
		private void dumpEdits()
		{
			this.changed = false;
			int dels = 0;
			int adds = 0;

			for (int i = 0; i < this.edits.Count; i++)
			{
				// calculate adds and deletes
				if (this.changes[i].Deleted || this.edits[i].Key == null && this.edits[i].Value == null)
				{
					dels++;
				}
				if (this.changes[i].Added) adds++;

				// set new values if any exist
				this.changes[i].Newkey = this.edits[i].Key;
				this.changes[i].Newval = this.edits[i].Value;
			}

			this.controller.ProcessChanges(this.changes.ToArray(), adds, dels);
		}
		/// <summary>
		/// Displays the Entry Mode of the UI, where entries can be added and removed
		/// </summary>
		/// <param name="load">Whether to (re)load everything from the filename stored, or just provide an empty list.</param>
		private void entryMode(bool load)
		{
			// empty the current list
			foreach (Gtk.Widget child in this.displayEditEntryList.Children)
			{
				this.displayEditEntryList.Remove(child);
			}
			this.edits = new List<EditEntry>();
			this.changes = new List<Change>();
			if (load) // loading the entries from a saved file
			{
				this.changes = this.controller.Get(true, false).ToList();
				this.changed = false;
				foreach (Change s in this.changes) // create a UI item for each change
				{
					EditEntry e = new EditEntry(s.Oldkey, s.Oldval);
					this.edits.Add(e);
					this.displayEditEntryList.PackStart(e, false, false, 0);
				}
			}

			this.hidePanels();
			this.panelEntryMode.Show();
			this.displayEditEntryList.ShowAll();
		}
		/// <summary>
		/// Hide all the panels
		/// </summary>
		private void hidePanels()
		{
			this.panelMain.Hide();
			this.panelScoreMode.Hide();
			this.panelEditFile.Hide();
			this.panelStudyMode.Hide();
			this.panelEntryMode.Hide();
			this.panelTestMode.Hide();
		}
		/// <summary>
		/// Hanler for when the close button of the window is clicked
		/// </summary>
		protected void OnDeleteEvent(object sender, DeleteEventArgs a)
		{
			if (this.controller.FileLoaded) this.controller.Save();
			this.Destroy();
			WindowManager.Deregister();
		}
		/// <summary>
		/// Handler for when the new file button is clicked
		/// </summary>
		protected void handlerNewFile(object sender, System.EventArgs e)
		{
			this.entryMode(false);
		}
		/// <summary>
		/// Handler for when the load file button is clicked
		/// </summary>
		protected void handlerLoadFile(object sender, System.EventArgs e)
		{
			Gtk.FileChooserDialog fc =
			new Gtk.FileChooserDialog("Open",
										this,
										FileChooserAction.Open,
										"Cancel", ResponseType.Cancel,
										"Open", ResponseType.Accept);
			fc.Modal = true;
			if (fc.Run() == (int)ResponseType.Accept) // display file chooser and get the file URI to give to the controller
			{
				this.controller.Load(fc.Filename);

				this.Title = "Soushin - " + fc.Filename;

				this.hidePanels();
				this.panelEditFile.Show();
			}
			fc.Destroy();
		}
		/// <summary>
		/// Handler for when the entry mode button is clicked
		/// </summary>
		protected void handlerEntryMode(object sender, System.EventArgs e)
		{
			this.entryMode(true);
		}
		/// <summary>
		/// Handler for when the study mode button is clicked
		/// </summary>
		protected void handlerStudyMode(object sender, System.EventArgs e)
		{
			if (!this.controller.FileLoaded) this.handlerSaveAs(sender, e); // please save before studying
			else
			{
				this.test = false;
				this.studyIndex = 0;
				this.studies = new List<StudyView>();

				// empty the study list
				foreach (Gtk.Widget child in this.displayStudyList.Children)
				{
					this.displayStudyList.Remove(child);
				}

				// get only relevant questions
				Change[] data = this.controller.Get(false);

				foreach (Change s in data) // create ui elements
				{
					if (!s.Hidden)
					{
						StudyView v = new StudyView(s.Oldkey, s.Oldval);
						this.studies.Add(v);
						this.displayStudyList.PackStart(v, true, true, 0);
					}
				}

				if (this.studies.Count == 0)
				{
					StudyView v = new StudyView("You have memorized all the questions!",
					"Press the Forget button to study again.");
					this.studies.Add(v);
					this.displayStudyList.PackStart(v, true, true, 0);
				}

				this.hidePanels();
				this.panelStudyMode.Show();
				this.displayStudyList.Show();
				this.studies[0].Show();
			}
		}
		/// <summary>
		/// Handler for when the test mode button is clicked
		/// </summary>
		protected void handlerTestMode(object sender, System.EventArgs e)
		{
			if (!this.controller.FileLoaded) this.handlerSaveAs(sender, e); // save before testing
			else
			{
				this.test = true;
				this.testIndex = 0;
				this.tests = new List<TestView>();

				// empty the test list
				foreach (Gtk.Widget child in this.displayTestList.Children)
				{
					this.displayTestList.Remove(child);
				}

				// get ALL questions
				Change[] data = this.controller.Get(true);

				// create UI elements
				foreach (Change s in data)
				{
					TestView v = new TestView(s.Oldkey, s.Index.GetValueOrDefault());
					this.tests.Add(v);
					this.displayTestList.PackStart(v, true, true, 0);
				}

				this.hidePanels();
				this.panelTestMode.Show();
				this.displayTestList.Show();
				this.tests[0].Show();
				this.tests[0].Focus();
			}
		}
		/// <summary>
		/// Handler for when the score mode button is clicked
		/// </summary>
		protected void handlerScoreMode(object sender, System.EventArgs e)
		{
			this.scores = new List<ScoreView>();

			// empty old scores
			foreach (Gtk.Widget child in this.displayScoreList.Children)
			{
				this.displayScoreList.Remove(child);
			}

			// create UI elements from test entries
			foreach (TestView t in tests)
			{
				ScoreView v = new ScoreView(t.Question, t.Answer, this.controller.GetCorrectAnswer(t.Index), t.Index);
				this.scores.Add(v);
				this.displayScoreList.PackStart(v, false, false, 0);
				v.Show();
			}

			this.hidePanels();
			this.panelScoreMode.Show();
		}
		/// <summary>
		/// Handler for when the saveas button is clicked
		/// </summary>
		protected void handlerSaveAs(object sender, System.EventArgs e)
		{
			this.processDeletes(); // handle elements that have been deleted in entry view
			if (this.changed) // anything to save?
			{
				Gtk.FileChooserDialog fc =
				new Gtk.FileChooserDialog("Save As",
											this,
											FileChooserAction.Save,
											"Cancel", ResponseType.Cancel,
											"Save", ResponseType.Accept);
				fc.Modal = true;
				if (fc.Run() == (int)ResponseType.Accept) // get the URI to save to
				{
					this.dumpEdits(); // dump all changes to prepare to serialize
					string fil = fc.Filename.EndsWith(".xml") ? fc.Filename : fc.Filename + ".xml";
					this.controller.Save(fil); // do the serialize
					this.Title = "Soushin - " + fc.Filename;
					this.handlerEntryMode(sender, e); // go back to entry mode so user can see the entries are now read-only
				}
				fc.Destroy();
			}
		}
		/// <summary>
		/// Handler for when the save button is clicked
		/// </summary>
		protected void handlerSaveFile(object sender, System.EventArgs e)
		{
			this.processDeletes(); // handle any deleted elements - this method is atomic, so no harm calling it twice if we end up calling SaveAs in a second
			if (this.controller.FileLoaded && this.changed)
			{
				this.dumpEdits();
				if (this.controller.CurrentLength > 0)
				{
					this.controller.Save(); // with previous filename
					this.handlerEntryMode(sender, e);
				}
			}
			else this.handlerSaveAs(sender, e);
		}
		/// <summary>
		/// Handler for when the new window button is clicked
		/// </summary>
		protected void handlerNewWindow(object sender, System.EventArgs e)
		{
			MainWindow m = new MainWindow();
			WindowManager.Register();
			m.Show();
		}
		/// <summary>
		/// Handler for when the new edit field button is clicked in the edit entries view
		/// </summary>
		protected void handlerNewEditField(object sender, System.EventArgs e)
		{
			EditEntry d = new EditEntry(null, null);
			this.displayEditEntryList.PackStart(d, false, false, 0);
			this.edits.Add(d);
			d.Show();

			this.changed = true;
			this.changes.Add(new Change());
			this.scrolledwindow1.Vadjustment.Value = this.scrolledwindow1.Vadjustment.Upper;
		}
		/// <summary>
		/// Handler for when the next question button is clicked
		/// </summary>
		protected void handlerNext(object sender, System.EventArgs e)
		{
			if (this.test) // if we're in test mode, advance the test list, else advance the study list
			{
				if (this.tests[testIndex].Answer != "")
				{
					this.tests[testIndex].Hide();
					testIndex++;
					if (testIndex >= this.tests.Count) // at the end of the test, show the scoring screen
					{
						this.handlerScoreMode(sender, e);
					}
					else
					{
						this.tests[testIndex].Show();
						this.tests[testIndex].Focus();
					}
				}
			}
			else // in study mode
			{
				this.studies[studyIndex].Hide();
				studyIndex++;
				if (studyIndex >= this.studies.Count) // if we reach the end in study mode, regenerate the list in a new random order for more studying
				{
					this.handlerStudyMode(sender, e);
				}
				else
				{
					this.studies[studyIndex].Show();
				}
			}
		}
		/// <summary>
		/// Handler for when the forget button is clicked in study mode
		/// </summary>
		protected void handlerForget(object sender, System.EventArgs e)
		{
			this.controller.Forget();
			this.handlerStudyMode(sender, e);
		}
		/// <summary>
		/// Handler for when the ok button is clicked in the score view and the user is finished scoring
		/// </summary>
		protected void handlerScore(object sender, System.EventArgs e)
		{
			bool[] process = new bool[this.controller.CurrentLength];
			foreach (ScoreView s in this.scores)
			{
				if (s.Score == null) // if the user has not scored a particular item, stop here
				{
					// TODO error message
					return;
				}
				else process[s.Index] = (bool)s.Score;
			}
			this.controller.Score(process);
			this.hidePanels();
			this.panelEditFile.Show();
		}
		/// <summary>
		/// Handler for when the cancel button is clicked in the score view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void handlerDiscardScore(object sender, System.EventArgs e)
		{
			this.hidePanels();
			this.panelEditFile.Show();
		}
	}
}