using System;

namespace Soushin
{
	/// <summary>
	/// UI element that handles displaying an entry as a test question
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class TestView : Gtk.Bin
	{
		/// <summary>
		/// The 'real' index, disregarding any randomization
		/// </summary>
		public int Index { get; private set; }
		public string Question { get; private set; }
		public string Answer { get { return this.entry1.Text; } } // the user's answer
		public TestView(string key, int index)
		{
			this.Question = key;
			this.Index = index;
			this.Build();
			this.label5.Markup = "<span font='30.0' weight='bold'>" + key + "</span>";
		}
		public void Focus()
		{
			this.entry1.GrabFocus();
		}
	}
}

