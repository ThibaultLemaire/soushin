namespace Soushin
{
	/// <summary>
	/// UI class for displaying the edit fields for one entry
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class EditEntry : Gtk.Bin
	{
		private string key;
		private string val;

		public string Key { 
			get { 
				return this.entry1.Text != "" 
					? this.entry1.Text 
					: this.key; 
			} 
		}
		public string Value { 
			get {
				return this.entry2.Text != "" 
					? this.entry2.Text 
					: this.val;
			}
		}
		/// <remarks>
		/// Currently, items are made read-only after the changes are saved, and no editing is performed.
		/// </remarks>
		public bool ReadOnly { get; private set; } // TODO add editing functionality
		public bool Deleted { get; private set; }

		/// <remarks>
		/// If both arguments are omitted, displays a dummy header for the other instances
		/// </remarks>
		public EditEntry(string key = "<b>Question</b>", string val = "<b>Answer</b>")
		{
			this.Build();

			this.Deleted = false;
			this.key = key;
			this.val = val;
			if (key == null && val == null) // No values yet, so show the text boxes instead of the labels
			{
				this.ReadOnly = false;
				this.label1.Destroy();
				this.label2.Destroy();
				this.label9.Destroy();
			}
			else // has values, so read-only
			{
				if (key == "<b>Question</b>" && val == "<b>Answer</b>") // dummy label row, so no management items
				{
					this.button1.Destroy();
					this.hseparator2.Destroy();
					this.label9.Markup = "<b>Remove</b>";
				}
				else // standard row, so hide extra label
				{
					this.label9.Destroy();
				}
				this.entry1.Destroy();
				this.entry2.Destroy();
				this.label1.Markup = key;
				this.label2.Markup = val;
			}
		}
		/// <summary>
		/// Handler for when the delete button is clicked
		/// </summary>
		protected void handlerDelete(object sender, System.EventArgs e)
		{
			this.Deleted = true;
			this.Hide();
		}
	}
}

