using System;

namespace Soushin
{
	/// <summary>
	/// This class represents the UI component which displays the scoring interface for one entry
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class ScoreView : Gtk.Bin
	{
		/// <summary>
		/// The 'real' index, disregarding any randomization
		/// </summary>
		public int Index { get; private set; }

		/// <summary>
		/// True if the user marked this question correct, false if they marked it wrong, null if it has not been marked
		/// </summary>
		public bool? Score
		{
			get
			{
				return this.radiobutton1.Active || this.radiobutton2.Active 
					? (bool?)this.radiobutton1.Active 
					: null;
			}
		}
		/// <remarks>
		/// If all arguments are omitted, displays a dummy header for the other instances
		/// </remarks>
		public ScoreView(string key = "<b>Question</b>", string val = "<b>Your Answer</b>", string tr = "<b>Correct Answer</b>", int index = -1)
		{
			this.Build();

			this.radiobutton1.Active = false;
			this.radiobutton2.Active = false;
			this.label6.Markup = key;
			this.label7.Markup = val;
			this.label8.Markup = tr;

			this.Index = index;

			if (key == "<b>Question</b>" && val == "<b>Your Answer</b>" && tr == "<b>Correct Answer</b>") // dummy header
			{
				this.radiobutton1.Destroy();
				this.radiobutton2.Destroy();
				this.hseparator1.Destroy();
			}
			else // actual entry
			{
				this.label1.Destroy();
				this.label2.Destroy();
				if (val == tr) // if the user's answer and the default answer are exactly the same, mark correct, otherwise the user will have to exercise their judgement on whether they got the answer about right
				{
					this.radiobutton1.Active = true;
				}
				else
				{
					this.radiobutton2.Active = true;
				}
			}
		}
	}
}

