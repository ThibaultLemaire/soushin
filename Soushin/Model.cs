using System;
using System.Xml.Serialization;

namespace Soushin
{
	/// <summary>
	/// This class represents the basic concept of an entry in the system and is used for the serialized representation
	/// </summary>
	public class Entry
	{
		public string Key { get; set; }
		public string Value { get; set; }
		public bool Hidden { get; set; }

		public Entry(string key, string val, bool hidden)
		{
			this.Key = key;
			this.Value = val;
			this.Hidden = hidden;
		}
		private Entry() { }
	}
	/// <summary>
	/// This class represents the concept of a change made to an entry. It serves as the intermediary between the View, which makes the changes, and the Controller, which deals with committing those changes to the proper data model. Note that unchanged entries are also stored in a Change instance - deleted and added will be false and the 'old' properties will equal the 'new' properties (or the corresponding text fields, if changes have not been dumped) in this instance
	/// </summary>
	public class Change
	{
		public int? Index { get; private set; } // null if newly added

		public bool Hidden { get; private set; }
		public bool Deleted { get; private set; }
		public bool Added { get; private set; }

		public string Oldkey { get; private set; }
		public string Oldval { get; private set; }
		public string Newkey { get; set; }
		public string Newval { get; set; }

		public void Delete()
		{
			this.Deleted = true;
		}
		public Change(int? index = null, string oldkey = null, string oldval = null)
		{
			if (oldkey == null && oldval == null && index == null)
			{
				Added = true;
				Hidden = false;
			}
			else if (oldkey == null && oldval == null && index != null)
			{
				Added = false;
				Hidden = true;
			}
			Deleted = false;
			this.Oldkey = oldkey;
			this.Oldval = oldval;
			this.Newkey = oldkey;
			this.Newval = oldval;
			this.Index = index;
		}
	}
}

