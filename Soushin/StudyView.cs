using System;

namespace Soushin
{
	/// <summary>
	/// UI element that handles displaying an entry in the study view
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class StudyView : Gtk.Bin
	{
		public StudyView(string key = "Question", string val = "Answer")
		{
			this.Build();
			this.label3.Markup = "<span font='30.0'>" + key + "</span>";
			this.label4.Markup = "<span font='30.0'>" + val + "</span>";
		}
	}
}

